#!/bin/env node

var express = require('express');
var fs = require('fs');
var mongoose = require('mongoose');

var mongoConnectString = "mongodb://localhost/agendae";
mongoose.connect(mongoConnectString);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log("MongoDB connection successful");
});

var un = 'node'; //change this to something private
var pw = 'password'; //change this to something private

var profissao = require("./model/profissao")
var endereco = require("./model/endereco")
var cliente = require("./model/cliente")
var avaliacao = require("./model/avaliacao")
var especialista = require("./model/especialista")


var nodeApp = function() {

    //  Scope.
    var self = this;


    /*  ================================================================  */
    /*  Helper functions.                                                 */
    /*  ================================================================  */

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
    self.setupVariables = function() {
        //  Set the environment variables we need.
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
        self.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;


        if (typeof self.ipaddress === "undefined") {
            //  Log errors but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "0.0.0.0";
        };
    };




    /**
     *  terminator === the termination handler
     *  Terminate server on receipt of the specified signal.
     *  @param {string} sig  Signal to terminate on.
     */
    self.terminator = function(sig) {
        if (typeof sig === "string") {
            console.log('%s: Received %s - terminating sample app ...',
                Date(Date.now()), sig);
            process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()));
    };


    /**
     *  Setup termination handlers (for exit and a list of signals).
     */
    self.setupTerminationHandlers = function() {
        //  Process on exit and signals.
        process.on('exit', function() {
            self.terminator();
        });

        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
            'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function(element, index, array) {
            process.on(element, function() {
                self.terminator(element);
            });
        });
    };


    /*  ================================================================  */
    /*  App server functions (main app logic here).                       */
    /*  ================================================================  */


    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     */
    self.initializeServer = function() {

        self.app = express();
        self.app.use(express.logger('dev'));
        self.app.use(express.bodyParser());
        self.app.use(express.static(__dirname + '/public'));
        var auth = express.basicAuth(un, pw);


        self.app.get('/api/test', auth, function(req, res) {
            console.log(req.headers);
            res.send([{ code: 2, name: "ken" }, { name: 'wine2' }]);
        });

        self.app.post('/api/test', function(req, res) {
            var reqBody = req.body;
            console.log("new: " + JSON.stringify(reqBody));
            res.json(req.body);
        });

        //API

        //self.app.get('/api/profissao', auth, profissao.listProfissao);
        self.app.get('/api/profissao', profissao.listProfissao);
        //self.app.post('/api/profissao', auth, profissao.listProfissao);
        self.app.post('/api/profissao', profissao.listProfissao);
        self.app.get('/api/profissao/:idProfissao', profissao.getProfissao);
        self.app.get('/api/especialista/:idProfissoes', especialista.listEspecialistas);
        self.app.get('/api/especialista/id/:idEspecialista', especialista.getEspecialista);
        self.app.post('/api/especialista/id/:idEspecialista', especialista.editEspecialista);
      //  self.app.get('/api/especialista/:idProfissoes', auth, especialista.listEspecialistas);

        //    db.especialista.find({profissoes_id:ObjectId("5a13365f7647acbbc60fbb11")})

        //   self.app.post('/api/especialista/perfil/:id', auth, especialista.findEspecialista);


        self.app.post('/api/cliente/login', cliente.login);
        self.app.post('/api/cliente', cliente.criar);
        self.app.get('/api/cliente/id/:idCliente', cliente.getCliente);
        self.app.post('/api/cliente/id/:idCliente', cliente.editCliente);

        self.app.post('/api/especialista/login', especialista.login);
        self.app.post('/api/especialista', especialista.criar);
        /*function(req, res) {
            var reqBody = req.body;
            var auth = false;

            if (!req.is('application/json')) {
                res.status(415);
            } else {
                if (reqBody.username === un && reqBody.password === pw) {
                    auth = true;
                    //status = 200;
                } else {
                    auth = false;
                    res.status(401);
                    //status = 401;
                }
            }
            res.send({ "authenticated": auth });
        });*/
    };

    /**
     *  Initializes the sample application.
     */
    self.initialize = function() {
        self.setupVariables();
        self.setupTerminationHandlers();

        // Create the express server and routes.
        self.initializeServer();
    };


    /**
     *  Start the server (starts up the sample application).
     */
    self.start = function() {
        //  Start the app on the specific interface (and port).
        self.app.listen(self.port, self.ipaddress, function() {
            console.log('%s: Node server started on %s:%d ...',
                Date(Date.now()), self.ipaddress, self.port);
        });
    };



};

/**
 *  main():  Main code.
 */
var zapp = new nodeApp();
zapp.initialize();
zapp.start();
