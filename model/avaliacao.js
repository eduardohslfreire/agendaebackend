var mongoose = require('mongoose');
var cliente = require('./cliente');

var avaliacaoSchema = new mongoose.Schema({
    score: Number,
    comentario: String,
    //cliente: { type: mongoose.Schema.Types.ObjectId, ref: cliente.clienteSchema }
  }, { collection: 'avaliacao' });

exports = mongoose.model("avaliacao", avaliacaoSchema);
