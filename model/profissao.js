var mongoose = require('mongoose');

var profissaoSchema = new mongoose.Schema({
    nome: String
}, { collection: 'profissao' });

exports.listProfissao = function(req, res) {
    var Profissao = mongoose.model('profissao', profissaoSchema);
    Profissao.find({}, function(err, results) {
        // res.send(results); for example.
        if (err) {
            res.status(500);
            res.send({ "success": false });
        }
        //console.log(results);
        console.log("profissaoList:" + JSON.stringify(results));
        res.status(200).json(results);
    });
};

exports.getProfissao = function(req, res) {
    var id = req.params.idProfissao;

    var Profissao = mongoose.model('profissao', profissaoSchema);
    Profissao.findOne({"_id":id}, function(err, results) {
        // res.send(results); for example.
        if (err) {
            res.status(500);
            res.send({ "success": false });
        }
        //console.log(results);
        console.log("profissao:" + JSON.stringify(results));
        res.status(200).json(results);
    });
};

exports = mongoose.model("profissao", profissaoSchema);
