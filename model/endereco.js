var mongoose = require('mongoose');

var enderecoSchema = new mongoose.Schema({
    logradouro: String,
    numero: Number,
    coordenadas: {
        type: [Number],
        default: [0, 0]
    },
    latitude: Number,
    tipo: String /* Estatico ou realtime*/
}, { collection: 'endereco' });

exports = mongoose.model("endereco", enderecoSchema);
