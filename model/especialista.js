var mongoose = require('mongoose');
var profissao = require('./profissao');
//var endereco = require('./endereco');
var avaliacao = require('./avaliacao');

var especialistaSchema = new mongoose.Schema({
    nome: String,
    telefone: String,
    cpf: String,
    email: String,
    usuario: String,
    senha: String,
    avaliacao: [{ type: mongoose.Schema.Types.ObjectId, ref: avaliacao.avaliacaoSchema }],
    endereco: {
        logradouro: String,
        numero: Number,
        bairro: String,
        cidade: String,
        cep: String
    },
    experiencias: String,
    profissoes_id: [{ type: mongoose.Schema.Types.ObjectId, ref: profissao.profissaoSchema }],
    horario_agenda : [{ hora_inicial: String,
                        hora_final: String,
                        dia_da_semana: Number}]
}, { collection: 'especialista' });

exports.criar = function(req, res) {
    var reqBody = req.body;
    var success = false;
    var id = -1;
    console.log(reqBody);
    if (req.is('application/json')) {
        var especialistaJson = JSON.stringify(reqBody);
        console.log("new Especialista: " + especialistaJson);
        var Especialista = mongoose.model('especialista', especialistaSchema);
        var esp = new Especialista(reqBody);

        esp.save(function(err) {
            if (err) {
                console.log("error: " + err);
                res.status(500);
                res.send(returnVal);
            }
        });
        success = true;
        id = esp._id;
    } else {
        res.status(415);
    }

    var returnVal = {
        "success": success,
        "id": id
    };
    res.send(returnVal);
};

exports.login = function(req, res) {
    var reqBody = req.body;
    var auth = false;

    if (!req.is('application/json')) {
        res.status(415);
    } else {
        var Especialitas = mongoose.model('especialista', especialistaSchema);
        Especialitas.findOne({"usuario": reqBody.username}, {}, function(err, results) {
          if (results !== null && results !== undefined) {
              if(results.senha == reqBody.password) {
                  auth = true;
                  res.send({ "authenticated": auth ,"perfil": results});
              } else {
                    auth = false;
                    res.status(401);
              }
          }
          res.send({ "authenticated": auth });
        });
    }

};

exports.listEspecialistas = function(req, res) {
    var id = req.params.idProfissoes;
    //console.log("id "+ id);

    if (id !== null && id !== undefined) {
        var Especialitas = mongoose.model('especialista', especialistaSchema);

        Especialitas.find({"profissoes_id": id}, function(err, results) {
            console.log("especialistas:" + JSON.stringify(results));
            res.send(results);
        });
    } else {
        res.status(500);
        res.send({ "success": false });
    }
};

exports.getEspecialista = function(req, res) {
    console.log(req.params);
    var id =  req.params.idEspecialista;

    if (id !== null && id !== undefined) {
        var Especialitas = mongoose.model('especialista', especialistaSchema);

        Especialitas.findOne({"_id":id}, function(err, results) {
            console.log(JSON.stringify(results));
            res.send(results);
        });
    } else {
        res.status(500);
        res.send({ "success": false });
    }
};

exports.editEspecialista = function(req, res) {
    console.log(req.params);
    console.log(req.body);
    var id =  req.params.idEspecialista;
    var reqBody = req.body;

    if (id !== null && id !== undefined) {
        var Especialitas = mongoose.model('especialista', especialistaSchema);

        Especialitas.findOne({"_id":id}, function(err, results) {        
            results.nome = reqBody.nome;
            results.email = reqBody.email;
            results.cpf = reqBody.cpf;
            results.usuario = reqBody.usuario;
            results.senha = reqBody.senha;
            results.telefone = reqBody.telefone;
            results.endereco.logradouro = reqBody.endereco.logradouro;
            results.endereco.numero = reqBody.endereco.numero;
            results.endereco.bairro = reqBody.endereco.bairro;
            results.endereco.cidade = reqBody.endereco.cidade; 
            results.endereco.cep = reqBody.endereco.cep;
            results.experiencias = reqBody.experiencias;
            results.profissoes_id = reqBody.profissoes_id;
            results.save(function (err, newResults) {
                res.send({ "success": true });
            });            
        });
    } else {
        res.status(500);
        res.send({ "success": false });
    }
};

/*
exports.saveBlog = function(req, res) {
    var reqBody = req.body;
    var success = false;
    var id = -1;
    console.log(reqBody);
    if (req.is('application/json')) {
        var blogJson = JSON.stringify(reqBody);
        console.log("new Blog: " + blogJson);
        var Blog = mongoose.model('Blog', blogSchema);
        var blg = new Blog(reqBody);

        blg.save(function(err) {
            if (err) {
                console.log("error: " + err);
                res.status(500);
                res.send(returnVal);
            }
        });
        success = true;
        id = blg._id;
    } else {
        res.status(415);
    }

    var returnVal = {
        "success": success,
        "id": id
    };
    res.send(returnVal);


};

exports.saveComment = function(req, res) {
    var reqBody = req.body;
    var success = false;
    var id = -1;
    console.log(reqBody);
    if (req.is('application/json')) {
        var commentJson = JSON.stringify(reqBody);
        console.log("new comment: " + commentJson);
        var Comment = mongoose.model('Comment', commentSchema);
        var cmt = new Comment(reqBody);

        cmt.save(function(err) {
            if (err) {
                console.log("error: " + err);
                res.status(500);
                res.send(returnVal);
            }
        });
        success = true;
        id = cmt._id;
    } else {
        res.status(415);
    }

    var returnVal = {
        "success": success,
        "id": id
    };
    res.send(returnVal);
};

exports.findBlogList = function(req, res) {
    var Blog = mongoose.model('Blog', blogSchema);
    Blog.find({}, function(err, results) {
        // res.send(results); for example.
        if (err) {
            res.status(500);
            res.send({ "success": false });
        }
        console.log("BlogList:" + JSON.stringify(results));
        res.send(results);
    });


};

exports.findBlog = function(req, res) {
    var id = req.params.id;
    if (id !== null && id !== undefined) {
        var Blog = mongoose.model('Blog', blogSchema);
        var Comment = mongoose.model('Comment', commentSchema);
        Blog.findById(id, function(err, results) {
            // res.send(results); for example.
            console.log("Blog:" + JSON.stringify(results));
            Comment.find({ "blog": id }, function(err, commentResults) {
                console.log("Comments:" + JSON.stringify(commentResults));
                var blog = results.toObject();
                blog.comments = commentResults;
                console.log("Blog2:" + JSON.stringify(blog));
                res.send(blog);
            });

        });
    } else {
        res.status(500);
        res.send({ "success": false });
    }

};
*/
