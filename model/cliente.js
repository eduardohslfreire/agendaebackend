var mongoose = require('mongoose');
//var endereco = require('./endereco');

var clienteSchema = new mongoose.Schema({
    nome: String,
    telefone: String,
    cpf: String,
    email: String,
    usuario: String,
    senha: String,
    endereco: {
        logradouro: String,
        numero: Number,
        bairro: String,
        cidade: String,
        cep: String
    }
}, { collection: 'cliente' });


exports.login = function(req, res) {
    var reqBody = req.body;
    var auth = false;
    var id = -1;

    if (!req.is('application/json')) {
        res.status(415);
    } else {
        var Clientes = mongoose.model('cliente', clienteSchema);
        Clientes.findOne({"usuario": reqBody.username}, {}, function(err, results) {
            if (results !== null && results !== undefined) {
                if (results.senha == reqBody.password) {
                    auth = true;
                    id = results._id;                    
                } else {
                    auth = false;
                    res.status(401);
                }
            }
            res.send({ "authenticated": auth , "id" : id});
        });
    }

};


exports.criar = function(req, res) {
    var reqBody = req.body;
    var success = false;
    var id = -1;
    console.log(reqBody);
    if (req.is('application/json')) {
        var clienteJson = JSON.stringify(reqBody);
        console.log("new Cliente: " + clienteJson);
        var Cliente = mongoose.model('cliente', clienteSchema);
        var cli = new Cliente(reqBody);

        cli.save(function(err) {
            if (err) {
                console.log("error: " + err);
                res.status(500);
                res.send(returnVal);
            }
        });
        success = true;
        id = cli._id;
    } else {
        res.status(415);
    }

    var returnVal = {
        "success": success,
        "id": id
    };
    res.send(returnVal);
};


exports.getCliente = function(req, res) {
    console.log(req.params);
    var id =  req.params.idCliente;

    if (id !== null && id !== undefined) {
        var Clientes = mongoose.model('cliente', clienteSchema);

        Clientes.findOne({"_id":id}, function(err, results) {
            console.log(JSON.stringify(results));
            res.send(results);
        });
    } else {
        res.status(500);
        res.send({ "success": false });
    }
};

exports.editCliente = function(req, res) {
    console.log(req.params);
    console.log(req.body);
    var id =  req.params.idCliente;
    var reqBody = req.body;

    if (id !== null && id !== undefined) {
        var Clientes = mongoose.model('cliente', clienteSchema);

        Clientes.findOne({"_id":id}, function(err, results) {        
            results.nome = reqBody.nome;
            results.email = reqBody.email;
            results.cpf = reqBody.cpf;
            results.usuario = reqBody.usuario;
            results.senha = reqBody.senha;
            results.telefone = reqBody.telefone;
            results.endereco.logradouro = reqBody.endereco.logradouro;
            results.endereco.numero = reqBody.endereco.numero;
            results.endereco.bairro = reqBody.endereco.bairro;
            results.endereco.cidade = reqBody.endereco.cidade; 
            results.endereco.cep = reqBody.endereco.cep;
            results.save(function (err, newResults) {
                res.send({ "success": true });
            });            
        });
    } else {
        res.status(500);
        res.send({ "success": false });
    }
};